#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER huacalero WITH PASSWORD 'ues';
	CREATE DATABASE huacalito WITH OWNER huacalero TEMPLATE template0;
	GRANT ALL PRIVILEGES ON DATABASE huacalito TO hucalero;
EOSQL