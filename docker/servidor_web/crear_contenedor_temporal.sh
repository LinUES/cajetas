#!/bin/bash
docker buildx build -t servidor_cajetas .
docker run\
 --name servidor_temporal\
 -p 127.0.0.1:80:8080\
 -v $PWD/../../:/var/www/localhost/htdocs/\
 -it\
 --rm\
 servidor_cajetas