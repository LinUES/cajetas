<?php
    class Conexion {

        private $enlace ;
        private $error  ;
        private $estado ;

        public function __construct( $configuracion = "" ) {
            if ( ! $configuracion ) {
                $configuracion = "./sys/conf/base.php" ;
            }
            if ( file_exists( $configuracion ) ) {
                include $configuracion ;
                $dsn = "" ;
                switch ( $TIPO ) {
                    case 'sqlite3' :
                        $ARCHIVO = ( $ARCHIVO ) ? $ARCHIVO : "./sys/var/db.sqlite3" ;
                        if ( file_exists( $ARCHIVO ) && is_writable( $ARCHIVO ) ) {
                            $dsn = "sqlite:$ARCHIVO" ;
                        } else {
                            $this->estado = false ;
                            $this->error  = "No se puede escribir en el archivo $ARCHIVO" ;
                        }
                        break;
                    case 'mariadb' :
                    case 'mysql'   :
                        $dsn = "mysql:host=$SERVIDOR;port=$PUERTO;dbname=$BASE;charset=utf8" ;
                        break;
                    case 'postgresql' :
                        $dsn = "pgsql:host=$SERVIDOR;port=$PUERTO;dbname=$BASE" ;
                        break;
                    default :
                        $this->estado = false ;
                        $this->error = "No se identificó el tipo de base de datos $TIPO" ;
                        break;
                }
                try {
                    $this->estado = $this->conectar( $TIPO, $dsn, $USUARIO, $CLAVE ) ;
                } catch ( Exception $e ) {
                    $this->error = $e->getMessage() ;
                }
            } else {
                $this->error = "No existe el archivo $configuracion" ;
            }
        }

        private function conectar( $tipo , $dsn , $usuario , $clave ) {
            $this->error = false ;
            try {
                $this->enlace = ( $tipo == 'sqlite3' ) ?
                    @ new PDO( $dsn ) :
                    @ new PDO( $dsn , $usuario , $clave )
                ;
                $this->enlace->setAttribute( PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION )      ;
                $this->enlace->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE , PDO::FETCH_ASSOC ) ;
                $this->enlace->setAttribute( PDO::ATTR_EMULATE_PREPARES , true ) ;
                if ( $tipo == 'sqlite3' ) {
                    $this->enlace->exec( "PRAGMA foreign_keys = ON;" ) ;
                }
            } catch ( Exception $e ) {
                $this->error = $e->getMessage() ;
            }
            return ( ! $this->error ) ;
        }

        public function consultar( $sql , $datos = array() ) {
            $resultado = array();
            if ( $this->estado == 1 ) {
                $this->error = false ;
                try {
                    $sentencia = $this->enlace->prepare( $sql ) ;
                    if ( $sentencia->execute( $datos ) ) {
                        $resultado = $sentencia->fetchAll( PDO::FETCH_ASSOC ) ;
                    } else {
                        $this->error = true ;
                    }
                } catch ( Exception $e ) {
                    $this->error = $e->getMessage() ;
                }
            }
            return $resultado;
        }

        public function ejecutar( $sql , $datos = array() ) {
            $resultado = false ;
            if ( $this->estado == 1 ) {
                $this->error = false ;
                try {
                    $sentencia = $this->enlace->prepare( $sql ) ;
                    if ( $sentencia->execute( $datos ) ) {
                        $resultado = true ;
                    } else {
                        $this->error = true ;
                    }
                } catch ( Exception $e ) {
                    $this->error = $e->getMessage() ;
                }
            }
            return $resultado ;
        }

        public function obtenerError() {
            return $this->error ;
        }

    }
?>