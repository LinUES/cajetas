<?php
    function plantilla( $archivo = "" , $_P = array() ) {
        $contenido = "";
        if ( file_exists( $archivo ) ) {
            $programa = substr( $archivo , 0 , - 3 ) . "php" ;
            if ( file_exists( $programa ) ) {
                include "$programa" ;
            }
            $arreglo = file( $archivo );
            foreach ( $arreglo as $linea ) {
                $contenido = $contenido . $linea ;
            }
            if ( isset( $_P ) && is_array( $_P ) ) {
                foreach ( $_P as $indice => $valor ) {
                    $contenido = str_replace( "{".$indice."}" , ( $valor ) ? $valor : "" , $contenido ) ;
                }
            }
        } else {
            $contenido = "<!-- ERROR: No existe el archivo \"$archivo\" -->";
        }
        return $contenido;
    }
?>