<?php
    $base       = new Conexion() ;
    $solicitud  = @ ( isset( $_REQUEST[ 'solicitud' ] ) ) ? $_REQUEST[ 'solicitud' ] : "portada" ;
    $id_cuenta  = @ $_SESSION[ 'id_cuenta'  ] ;
    $id_entidad = @ $_SESSION[ 'id_entidad' ] ;
    $contenido  = "" ;

    $paginas    = [
        "portada"  => "publico/contenido" ,
        "entrar"   => "std/entrada"
    ] ;

    $sql = "
        select
            aplicacion,
            etiqueta,
            archivo,
            icono,
            peso
        from
            aplicacion
        where
            publica = true and
            activa = true
        order by
            peso
    ";

    $consulta = $base->consultar( $sql ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $aplicacion ) {
            #$paginas[ $aplicacion[ 'aplicacion' ] ] = "publico/" . $aplicacion[ 'archivo' ] . "/app";
            $paginas[ $aplicacion[ 'aplicacion' ] ] = "publico/" . $aplicacion[ 'archivo' ] ;
        }
        $consulta = "" ;
    }

    if ( isset( $_SESSION['usuario'] ) ){
        $solicitud = ( $solicitud == "entrar" ) ?  "portada" : $solicitud ;

        $paginas[ "privado"  ] = "privado/caratula";
        $paginas[ "personal" ] = "privado/yo"      ;
        $paginas[ "salir"    ] = "std/salida"      ;

        if ( $_SESSION[ 'id_entidad' ] == 0 ){
            $paginas[ "entidades"    ] = "root/crud-entidades"    ;
            $paginas[ "roles"        ] = "root/crud-roles"        ;
            $paginas[ "aplicaciones" ] = "root/crud-aplicaciones" ;
            $paginas[ "usuarios"     ] = "root/crud-usuarios"     ;
        } else {
            $sql = "
                select distinct
                    aplicacion,
                    etiqueta,
                    archivo,
                    icono,
                    peso
                from
                    aplicaciones_de_entidades
                where
                    id_rol in (
                        select
                            id_rol
                        from
                            roles_de_cuentas where
                        id_cuenta = ?
                    ) and
                    id_entidad = ? and
                    publica = false and
                    activa = true
                order by
                    peso
            ";
            $consulta = $base->consultar( $sql , [ $id_cuenta , $id_entidad ] ) ;
            foreach ( $consulta as $aplicacion ){
                $paginas[ $aplicacion[ 'aplicacion' ] ] = "privado/" . $aplicacion[ 'archivo' ] . "/app";
            }
        }

        if ( $solicitud == "portada" ){
            $solicitud = "privado";
        }
    }

    $contenido = ( isset( $paginas[ $solicitud ] ) ) ? $paginas[ $solicitud ] : "std/error404" ;

    if ( file_exists( "./sys/mvc/mv/$contenido.tpl" ) ){
        echo plantilla( "./sys/mvc/mv/$contenido.tpl" );
    } else {
        echo plantilla( "./sys/mvc/mv/std/error505.tpl" );
    }

?>