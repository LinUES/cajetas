<div class='row flex-center'>
    <div class='sm-12 md-4 col align-middle'>
        <div class='card'>
            <img class='mb-5' src='./usr/img/avatar.png' alt='...'>
            <div class='card-header'>
                <h1 class='card-title'>
                    Gato Barato
                </h1>
            </div>
            <div class='card-body'>
                <p class='card-text'>
                    Un gato que le gusta usar Debian GNU / linux.
                    <i class='lab la-linux'></i>
                </p>
                <p class='card-text'>
                    Pueden contactarme por telegram.
                    <i class='lab la-telegram'></i>
                </p>
                <p class='card-text'>
                    O invitarme a un sandwich con pollo.
                    <i class='las la-bread-slice'></i>
                    <i class='las la-drumstick-bite'></i>
                </p>
            </div>
        </div>
    </div>
</div>