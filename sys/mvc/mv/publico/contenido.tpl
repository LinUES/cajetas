<!-- Contenido -->

<h2>
    Portafolio
</h2>
<!-- Elementos del portafolio -->
<div class='row'>
    <!-- Elemento 1 -->
    <div class ='sm-12 md-6 col'>
        <div class='row flex-spaces child-borders'>
            <label class='paper-btn margin' for='modal01'>
                <img class='float-left' src='./usr/img/cabin.png' alt='una casa' width='200'>
            </label>
            <input class='modal-state' id='modal01' type='checkbox'>
            <div class='modal'>
                <label class='modal-bg' for='modal01'>
                </label>
                <div class='modal-body'>
                    <div class='container'>
                        <label class='btn-close' for='modal01'>
                            <i class='las la-window-close text-danger'></i>
                        </label>
                        <h2 class='modal-title'>
                            Mi Casa
                        </h2>
                        <img class='float-left' src='./usr/img/cabin.png' alt='una casa' width='50%'>
                        <p class='modal-text'>
                            Un lugar para disfrutar, la casa es dónde debemos estar, porque estar en la casa es la mejor forma de descansar :)
                        </p>
                        <label class='paper-btn btn-danger-outline' for='modal01'>
                            Regresar
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Elemento 2-->
    <div class='sm-12 md-6 col'>
        <div class='row flex-spaces child-borders'>
            <label class='paper-btn margin' for='modal02'>
                <img class='float-left' src='./usr/img/game.png' alt='un control' width='200'>
            </label>
            <input class='modal-state' id='modal02' type='checkbox'>
            <div class='modal'>
                <label class='modal-bg' for='modal02'>
                </label>
                <div class='modal-body'>
                    <div class='container'>
                        <label class='btn-close' for='modal02'>
                            <i class='las la-window-close text-danger'></i>
                        </label>
                        <h2 class='modal-title'>
                            Juego
                        </h2>
                        <img class='float-left' src='./usr/img/game.png' alt='un control' width='50%'>
                        <p class='modal-text'>
                            Jugar es saludable para la mente, cuando no es en exceso, todo es bueno pero con medida, y para entretenerse y divertirse es agradable disfrutar de un juego con amigos ;)
                        </p>
                        <label class='paper-btn btn-danger-outline' for='modal02'>
                            Regresar
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>