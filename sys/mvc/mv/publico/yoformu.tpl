<form method='post'>
    <div class='container'>
        <h3 class='text-center'>
            Formulario de contacto
        </h3>
        <div class='row flex-center'>
            <div class='md-6 sm-12 col'>
                <div class='form-group'>
                    <label for="nombre" class="form-label">
                        Nombre
                    </label>
                    <input type="text" class="input-block" id="nombre" placeholder="Pablo Perez de los Palotes">
                </div>
            </div>
        </div>
        <div class='row flex-center'>
            <div class='md-6 sm-12 col'>
                <div class='form-group'>
                    <label for="correo" class="form-label">
                        Correo
                    </label>
                    <input type="email" class="input-block" id="correo" placeholder="pablo@delos.palotes">
                </div>
            </div>
        </div>
        <div class='row flex-center'>
            <div class='md-6 sm-12 col'>
                <div class='form-group'>
                    <label for="mensaje" class="form-label">
                        Mensaje
                    </label>
                    <textarea class="input-block" id="mensaje" rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>
</form>