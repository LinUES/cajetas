<!-- Registro de una entidad -->
<tr>
    <td>
        {APLICACION}
    </td>
    <td>
        <a href='./?solicitud=aplicaciones&id_aplicacion={IDAPLICACION}&accion=roles' class='paper-btn btn-primary btn-small'>
            <i class='las la-user-tag'></i>
        </a>
        <a href='./?solicitud=aplicaciones&id_aplicacion={IDAPLICACION}&accion=ED' class='paper-btn btn-secondary btn-small'>
            <i class='las la-pen'></i>
        </a>
        <a href='./?solicitud=aplicaciones&id_aplicacion={IDAPLICACION}&accion=BO' class='paper-btn btn-small btn-danger'>
            <i class='las la-trash-alt'></i>
        </a>
    </td>
</tr>