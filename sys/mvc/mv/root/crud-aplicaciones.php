<?php
    $listaaplicaciones   = ""                          ;
    $base                = New Conexion()              ;
    $consulta            = ""                          ;
    $insertar            = ""                          ;
    $editar              = ""                          ;
    $borrar              = ""                          ;
    $sql                 = ""                          ;
    $id_aplicacion       = @ $_REQUEST[ 'id_aplicacion' ] ;
    $aplicacion          = @ $_REQUEST[ 'aplicacion'    ] ;
    $etiqueta            = @ $_REQUEST[ 'etiqueta'      ] ;
    $archivox            = @ $_REQUEST[ 'archivox'      ] ;
    $peso                = @ $_REQUEST[ 'peso'          ] ;
    $icono               = @ $_REQUEST[ 'icono'         ] ;
    $id_entidad          = @ $_REQUEST[ 'id_entidad'    ] ;
    $id_rol              = @ $_REQUEST[ 'id_rol'        ] ;
    $seccion             = @ ( $_REQUEST[ 'seccion' ] ) ? 'true' : 'false' ;
    $publica             = @ ( $_REQUEST[ 'publica' ] ) ? 'true' : 'false' ;
    $activa              = @ ( $_REQUEST[ 'activa'  ] ) ? 'true' : 'false' ;
    $ch_seccion          = "" ;
    $ch_publica          = "" ;
    $ch_activa           = "" ;
    $accion              = @ $_REQUEST[ 'accion' ]     ;
    $boton               = "" ;
    $color               = "" ;
    $bloqueo             = "" ;
    $listaentidades      = "" ;
    $listaroles          = "" ;
    $idaplicacionl       = "" ;
    $aplicacionr         = "" ;
    $listarolasignados   = "" ;
    $limpiar             = false ;
    $abremodal           = "" ;

    if ( ! $accion ) {
        $limpiar = true ;
    } else {
        switch ( $accion ) {
            case "ED":
            case "BO":
                $sql = "select * from aplicacion where id_aplicacion = ?" ;
                $consulta = $base->consultar( $sql , [ $id_aplicacion ] ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    $aplicacion = $consulta[0][ 'aplicacion' ] ;
                    $etiqueta   = $consulta[0][ 'etiqueta'   ] ;
                    $archivox   = $consulta[0][ 'archivo'    ] ;
                    $peso       = $consulta[0][ 'peso'       ] ;
                    $icono      = $consulta[0][ 'icono'      ] ;
                    $seccion    = $consulta[0][ 'seccion'    ] ;
                    $publica    = $consulta[0][ 'publica'    ] ;
                    $activa     = $consulta[0][ 'activa'     ] ;
                    $ch_seccion = ( $consulta[0][ 'seccion' ] === true ) ? "checked" : "" ;
                    $ch_publica = ( $consulta[0][ 'publica' ] === true ) ? "checked" : "" ;
                    $ch_activa  = ( $consulta[0][ 'activa'  ] === true ) ? "checked" : "" ;
                }
                $boton   = ( $accion == "ED" ) ? "Editar"    : "Borrar" ;
                $color   = ( $accion == "ED" ) ? "secondary" : "danger" ;
                $bloqueo = ( $accion == "BO" ) ? "disabled"  : ""       ;
                $accion  = ( $accion == "ED" ) ? "editar"    : "borrar" ;
                break ;
            case "agregar":
                $sql = "
                    insert into
                    aplicacion (
                        aplicacion ,
                        etiqueta   ,
                        archivo    ,
                        peso       ,
                        icono      ,
                        seccion    ,
                        publica    ,
                        activa
                    )
                    values (
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?,
                        ?
                    )
                " ;
                $insertar = $base->ejecutar(
                    $sql ,
                    [
                        $aplicacion ,
                        $etiqueta   ,
                        $archivox   ,
                        $peso       ,
                        $icono      ,
                        $seccion    ,
                        $publica    ,
                        $activa
                    ]
                ) ;

                if ( $insertar ) {
                    $limpiar = true ;
                }
                break ;
            case "editar":
                $sql = "
                    update aplicacion
                    set
                        aplicacion = ? ,
                        etiqueta   = ? ,
                        archivo    = ? ,
                        peso       = ?,
                        icono      = ?,
                        seccion    = ?,
                        publica    = ?,
                        activa     = ?
                    where
                        id_aplicacion = ?
                ";
                $editar = $base->ejecutar( $sql , [ $aplicacion , $etiqueta , $archivox , $peso , $icono , $seccion , $publica , $activa , $id_aplicacion ] ) ;
                $mostrar = $base->obtenerError() ;
                if ( $editar ) {
                    $limpiar = true ;
                } else {
                    $boton = "Editar"    ;
                    $color = "secondary" ;
                }
                break ;
            case "borrar":
                $sql = "delete from aplicacion where id_aplicacion = ?" ;
                $borrar = $base->ejecutar( $sql , [ $id_aplicacion ] ) ;
                if ( $borrar ) {
                    $limpiar = true ;
                } else {
                    $boton   = "Borrar"   ;
                    $color   = "danger"   ;
                    $bloqueo = "disabled" ;
                }
                break ;
            case "roles":
            case "roles-agregar" :
            case "roles-borrar" :
                if ( $accion == "roles-agregar" ) {
                    $sql = "
                        insert into
                        aplicacion_de_entidad (
                            id_aplicacion,
                            id_rol,
                            id_entidad
                        )
                        values(
                            ?,
                            ?,
                            ?
                        )
                    ";
                    $insertar = $base->ejecutar(
                        $sql ,
                        [
                            $id_aplicacion ,
                            $id_rol    ,
                            $id_entidad
                        ]
                    ) ;
                }
                if ( $accion == "roles-borrar" ) {
                    $sql = "
                        delete from
                            aplicacion_de_entidad
                        where
                            id_aplicacion  = ? and
                            id_rol     = ? and
                            id_entidad = ?
                    ";
                    $borrar = $base->ejecutar(
                        $sql ,
                        [
                            $id_aplicacion ,
                            $id_rol    ,
                            $id_entidad
                        ]
                    ) ;
                }
                $sql      = "select * from entidad" ;
                $consulta = $base->consultar( $sql ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    foreach ( $consulta as $entidadl ) {
                        $listaentidades .= plantilla(
                            "./sys/mvc/mv/root/crud-aplicaciones-lista-entidades.tpl" ,
                            [
                                "IDENTIDAD" => $entidadl[ 'id_entidad' ] ,
                                "ENTIDAD"   => $entidadl[ 'entidad'    ]
                            ]
                        );
                    }
                }
                $sql = "select * from rol";
                $consulta = $base->consultar( $sql ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    foreach ( $consulta as $roll ) {
                        $listaroles .= plantilla(
                            "./sys/mvc/mv/root/crud-aplicaciones-lista-roles.tpl" ,
                            [
                                "IDROL" => $roll[ 'id_rol' ] ,
                                "ROL"   => $roll[ 'rol'    ]
                            ]
                        );
                    }
                }
                $sql         = "select aplicacion from aplicacion where id_aplicacion = ?" ;
                $consulta    = $base->consultar( $sql , [ $id_aplicacion ] ) ;
                $aplicacionr = $consulta[0]['aplicacion'] ;
                $sql         = "select * from aplicaciones_de_entidades where id_aplicacion = ?" ;
                $consulta    = $base->consultar( $sql , [ $id_aplicacion ] ) ;
                foreach ( $consulta as $asignados ) {
                    $listarolasignados .= plantilla(
                        "./sys/mvc/mv/root/crud-aplicaciones-lista-asignados.tpl" ,
                        [
                            "IDAPLICACION" => $id_aplicacion ,
                            "IDROL"        => $asignados[ 'id_rol'     ],
                            "ROL"          => $asignados[ 'rol'        ],
                            "IDENTIDAD"    => $asignados[ 'id_entidad' ],
                            "ENTIDAD"      => $asignados[ 'entidad'    ]
                        ]
                    );
                }
                $idaplicacionl = $id_aplicacion ;
                $limpiar = true ;
                $abremodal = plantilla( "./sys/mvc/mv/root/crud-aplicaciones-modal.tpl" ) ;
                break ;
        }
    }

    if ( $limpiar ) {
        $id_aplicacion = "" ;
        $aplicacion    = "" ;
        $etiqueta      = "" ;
        $archivox      = "" ;
        $peso          = "" ;
        $icono         = "" ;
        $seccion       = "" ;
        $ch_seccion    = "" ;
        $publica       = "" ;
        $ch_publica    = "" ;
        $activa        = "" ;
        $ch_activa     = "checked" ;
        $accion        = "agregar" ;
        $boton         = "Agregar" ;
        $color         = "success" ;
    }

    $sql = "select * from aplicacion" ;
    $consulta = $base->consultar( $sql ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $registro ) {
            $listaaplicaciones .= plantilla(
                "./sys/mvc/mv/root/crud-aplicaciones-lista.tpl" ,
                [
                    "IDAPLICACION"    => $registro[ 'id_aplicacion' ] ,
                    "APLICACION"      => $registro[ 'aplicacion'    ]
                ]
            );
        }
    }


    $_P[ 'APLICACIONES'      ] = $listaaplicaciones  ;
    $_P[ 'ACCION'            ] = $accion        ;
    $_P[ 'BOTON'             ] = $boton         ;
    $_P[ 'COLOR'             ] = $color         ;
    $_P[ 'BLOQUEO'           ] = $bloqueo       ;
    $_P[ 'IDaplicacion'      ] = ( $id_aplicacion ) ? $id_aplicacion : "" ;
    $_P[ 'APLICACION'        ] = ( $aplicacion    ) ? $aplicacion    : "" ;
    $_P[ 'ETIQUETA'          ] = ( $etiqueta      ) ? $etiqueta      : "" ;
    $_P[ 'ARCHIVOX'          ] = ( $archivox      ) ? $archivox      : "" ;
    $_P[ 'SECCION'           ] = ( $seccion       ) ? $seccion       : "" ;
    $_P[ 'PESO'              ] = ( $peso          ) ? $peso          : "" ;
    $_P[ 'ICONO'             ] = ( $icono         ) ? $icono         : "" ;
    $_P[ 'PUBLICA'           ] = ( $publica       ) ? $publica       : "" ;
    $_P[ 'ACTIVA'            ] = ( $activa        ) ? $activa        : "" ;
    $_P[ 'CHEQUEADOSECCION'  ] = $ch_seccion ;
    $_P[ 'CHEQUEADOPUBLICA'  ] = $ch_publica ;
    $_P[ 'CHEQUEADOACTIVA'   ] = $ch_activa  ;
    $_P[ 'LISTAENTIDADES'    ] = $listaentidades ;
    $_P[ 'LISTAROLES'        ] = $listaroles     ;
    $_P[ 'IDAPLICACIONL'     ] = $idaplicacionl      ;
    $_P[ 'APLICACIONR'       ] = $aplicacionr        ;
    $_P[ 'LISTAROLASIGNADOS' ] = $listarolasignados ;
    $_P[ 'ABREMODAL'         ] = $abremodal      ;
    $_P[ 'MOSTRAR'           ] = "" ; //print_r($error, true) ;

?>