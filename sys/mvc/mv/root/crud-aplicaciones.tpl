<!-- CRUD DE aplicaciones -->
 <h1>
    Aplicaciones
</h1>
{MOSTRAR}
<div class='row'>
    <div class='sm-12 md-12 lg-6'>
        <form method='post'>
            <input type='hidden' name='solicitud' value='aplicaciones' >
            <input type='hidden' name='id_cuenta' value='{IDaplicacion}' >
            <div class='row'>
                <div class='col md-4'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='aplicacion' placeholder='aplicacion' name='aplicacion' value='{APLICACION}' {BLOQUEO} required>
                    </div>
                </div>
                <div class='col md-4'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='etiqueta' placeholder='etiqueta' name='etiqueta' value='{ETIQUETA}' {BLOQUEO} required>
                    </div>
                </div>
                <div class='col md-4'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='aplicacion' placeholder='archivo' name='archivox' value='{ARCHIVOX}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-6'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='aplicacion' placeholder='peso' name='peso' value='{PESO}' {BLOQUEO} required>
                    </div>
                </div>
                <div class='col md-6'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='aplicacion' placeholder='icono' name='icono' value='{ICONO}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-4'>
                    <div class='form-group'>
                        <label for='seccion' class='paper-check'>
                            <input type='checkbox' id='seccion' name='seccion' value='seccion' {CHEQUEADOSECCION} {BLOQUEO}>
                            <span>Sección</span>
                        </label>
                    </div>
                </div>
                <div class='col md-4'>
                    <div class='form-group'>
                        <label for='publica' class='paper-check'>
                            <input type='checkbox' id='publica' name='publica' value='publica' {CHEQUEADOPUBLICA} {BLOQUEO}>
                            <span>Pública</span>
                        </label>
                    </div>
                </div>
                <div class='col md-4'>
                    <div class='form-group'>
                        <label for='activa' class='paper-check'>
                            <input type='checkbox' id='activa' name='activa' value='activa' {CHEQUEADOACTIVA} {BLOQUEO}>
                            <span>Activa</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class='row flex-spaces'>
                <div class='col-5'>
                    <input type='hidden' name='accion' value='{ACCION}'>
                    <input type='submit' class='paper-btn btn-{COLOR} btn-block' value='{BOTON}'>
                </div>
                <div class='col-5 align-middle'>
                    <a href='./' class='paper-btn btn-warning btn-block'>
                        <center>
                            Cancelar
                        </center>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class='sm-12 md-12 lg-6'>
        <table class='table-hover'>
            <thead>
                <tr>
                    <th>
                        Aplicacion
                    </th>
                    <th>
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                {APLICACIONES}
            </tbody>
        </table>
    </div>
</div>
<div>
    <input class='modal-state' id='modal-1' type='checkbox'>
    <div class='modal'>
        <label class='modal-bg' for='modal-1'>

        </label>
        <div class='modal-body'>
            <div class='container'>
                <label class='btn-close' for='modal-1'>
                    <i class='las la-window-close text-danger'></i>
                </label>
                <h4 class='modal-title'>
                    Roles de Aplicación {APLICACIONR}
                </h4>
                <form method='post' class=''>
                    <input type='hidden' name='solicitud' value='aplicaciones'      >
                    <input type='hidden' name='accion'    value='roles-agregar' >
                    <input type='hidden' name='id_aplicacion' value='{IDAPLICACIONL}'   >
                    <div class='row'>
                        <div class='col'>
                            <h5 class='modal-subtitle'>
                                Entidades
                            </h5>
                            <select name='id_entidad'>
                                {LISTAENTIDADES}
                            </select>
                        </div>
                        <div class='col'>
                            <h5 class='modal-subtitle'>
                                Roles
                            </h5>
                            <select name='id_rol'>
                                {LISTAROLES}
                            </select>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col'>
                            <input type='submit' class='paper-btn btn-small btn-success-outline' value='Asignar'>
                        </div>
                    </div>
                </form>
                <h5 class='modal-subtitle'>
                    Roles asigandos
                </h5>
                <div>
                    <table>
                        {LISTAROLASIGNADOS}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{ABREMODAL}