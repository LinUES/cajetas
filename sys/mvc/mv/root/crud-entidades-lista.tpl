<!-- Registro de una entidad -->
<tr>
    <td>
        {ENTIDAD}
    </td>
    <td>
        {NOMBRE}
    </td>
    <td>
        {DESCRIPCION}
    </td>
    <td>
        <a href='./?solicitud=entidades&id_entidad={IDENTIDAD}&accion=ED' class='paper-btn btn-secondary btn-small'>
            <i class='las la-pen'></i>
        </a>
        <a href='./?solicitud=entidades&id_entidad={IDENTIDAD}&accion=BO' class='paper-btn btn-danger btn-small'>
            <i class='las la-trash-alt'></i>
        </a>
    </td>
</tr>