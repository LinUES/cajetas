<?php
    $listaEntidades      = ""                                   ;
    $base                = New Conexion()                       ;
    $consulta            = ""                                   ;
    $insertar            = ""                                   ;
    $editar              = ""                                   ;
    $borrar              = ""                                   ;
    $sql                 = ""                                   ;
    $id_entidad          = @ $_REQUEST[ 'id_entidad'          ] ;
    $entidad             = @ $_REQUEST[ 'entidad'             ] ;
    $nombre_entidad      = @ $_REQUEST[ 'nombre_entidad'      ] ;
    $descripcion_entidad = @ $_REQUEST[ 'descripcion_entidad' ] ;
    $accion              = @ $_REQUEST[ 'accion' ]              ;
    $boton               = "" ;
    $color               = "" ;
    $bloqueo             = "" ;
    $limpiar             = false ;

    if ( ! $accion ) {
        $limpiar = true ;
        /*
        $accion = "agregar" ;
        $boton  = "Agregar" ;
        $color  = "success" ;
        */
    } else {
        switch ( $accion ) {
            case "ED":
            case "BO":
                $sql = "select * from entidad where id_entidad = ?" ;
                $consulta = $base->consultar( $sql , [ $id_entidad ] ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    $entidad             = $consulta[0][ 'entidad'             ] ;
                    $nombre_entidad      = $consulta[0][ 'nombre_entidad'      ] ;
                    $descripcion_entidad = $consulta[0][ 'descripcion_entidad' ] ;
                }
                $boton   = ( $accion == "ED" ) ? "Editar"    : "Borrar" ;
                $color   = ( $accion == "ED" ) ? "secondary" : "danger" ;
                $bloqueo = ( $accion == "BO" ) ? "disabled"  : ""       ;
                $accion  = ( $accion == "ED" ) ? "editar"    : "borrar" ;
                break ;
            case "agregar":
                $sql = "
                    insert into
                    entidad (
                        entidad,
                        nombre_entidad,
                        descripcion_entidad
                    )
                    values (
                        ?,
                        ?,
                        ?
                    )
                " ;
                $insertar = $base->ejecutar(
                    $sql ,
                    [
                        $entidad             ,
                        $nombre_entidad      ,
                        $descripcion_entidad
                    ]
                ) ;
                if ( $insertar ) {
                    $limpiar = true ;
                }
                break ;
            case "editar":
                $sql = "
                    update entidad
                    set
                        entidad             = ? ,
                        nombre_entidad      = ? ,
                        descripcion_entidad = ?
                    where
                        id_entidad = ?
                ";
                $editar = $base->ejecutar( $sql , [ $entidad , $nombre_entidad , $descripcion_entidad , $id_entidad ] ) ;
                if ( $editar ) {
                    $limpiar = true ;
                } else {
                    $boton = "Editar"    ;
                    $color = "secondary" ;
                }
                break ;
            case "borrar":
                $sql = "delete from entidad where id_entidad = ?" ;
                $borrar = $base->ejecutar( $sql , [ $id_entidad ] ) ;
                if ( $borrar ) {
                    $limpiar = true ;
                } else {
                    $boton   = "Borrar"   ;
                    $color   = "danger"   ;
                    $bloqueo = "disabled" ;
                }
                break ;
        }
    }

    if ( $limpiar ) {
        $id_entidad          = "" ;
        $entidad             = "" ;
        $nombre_entidad      = "" ;
        $descripcion_entidad = "" ;
        $accion              = "agregar" ;
        $boton               = "Agregar" ;
        $color               = "success" ;
    }

    $sql = "select * from entidad" ;
    $consulta = $base->consultar( $sql ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $registro ) {
            $listaEntidades .= plantilla(
                "./sys/mvc/mv/root/crud-entidades-lista.tpl" ,
                [
                    "IDENTIDAD"   => $registro[ 'id_entidad'          ] ,
                    "ENTIDAD"     => $registro[ 'entidad'             ] ,
                    "NOMBRE"      => $registro[ 'nombre_entidad'      ] ,
                    "DESCRIPCION" => $registro[ 'descripcion_entidad' ]
                ]
            );
        }
    }

    $_P[ 'ENTIDADES'   ] = $listaEntidades      ;
    $_P[ 'ACCION'      ] = $accion              ;
    $_P[ 'BOTON'       ] = $boton               ;
    $_P[ 'COLOR'       ] = $color               ;
    $_P[ 'BLOQUEO'     ] = $bloqueo             ;
    $_P[ 'IDENTIDAD'   ] = ( $id_entidad          ) ? $id_entidad          : "" ;
    $_P[ 'ENTIDAD'     ] = ( $entidad             ) ? $entidad             : "" ;
    $_P[ 'NOMBRE'      ] = ( $nombre_entidad      ) ? $nombre_entidad      : "" ;
    $_P[ 'DESCRIPCION' ] = ( $descripcion_entidad ) ? $descripcion_entidad : "" ;
    $_P[ 'MOSTRAR'     ] = "" ; // print_r($_REQUEST, true) ;
?>