<!-- CRUD DE ENTIDADES -->
 <h1>
    ENTIDADES
</h1>
{MOSTRAR}
<div class='row'>
    <div class='sm-12 md-12 lg-6'>
        <form method='post'>
            <input type='hidden' name='solicitud' value='entidades' >
            <input type='hidden' name='id_entidad' value='{IDENTIDAD}' >
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='entidad' placeholder='Entidad' name='entidad' value='{ENTIDAD}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='nombre_entidad' placeholder='Nombre de la entidad' name='nombre_entidad' value='{NOMBRE}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <textarea id='descipcion_entidad' placeholder='Descripción de la entidad' name='descripcion_entidad' class='input-block' style='width: 100%;'  {BLOQUEO} required>{DESCRIPCION}</textarea>
                    </div>
                </div>
            </div>
            <div class='row flex-spaces'>
                <div class='col-5'>
                    <input type='hidden' name='accion' value='{ACCION}'>
                    <input type='submit' class='paper-btn btn-{COLOR} btn-block' value='{BOTON}'>
                </div>
                <div class='col-5 align-middle'>
                    <a href='./' class='paper-btn btn-warning btn-block'>
                        <center>
                            Cancelar
                        </center>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class='sm-12 md-12 lg-6'>
        <table class='table-hover'>
            <thead>
                <tr>
                    <th>
                        Entidad
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Descripción
                    </th>
                    <th>
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                {ENTIDADES}
            </tbody>
        </table>
    </div>
</div>
