<!-- Registro de una rol -->
<tr>
    <td>
        {ROL}
    </td>
    <td>
        {DESCRIPCION}
    </td>
    <td>
        <a href='./?solicitud=roles&id_rol={IDrol}&accion=ED' class='paper-btn btn-secondary btn-small'>
            <i class='las la-pen'></i>
        </a>
        <a href='./?solicitud=roles&id_rol={IDrol}&accion=BO' class='paper-btn btn-danger btn-small'>
            <i class='las la-trash-alt'></i>
        </a>
    </td>
</tr>