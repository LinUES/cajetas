<?php
    $listaroles      = ""                               ;
    $base            = New Conexion()                   ;
    $consulta        = ""                               ;
    $consultarol     = ""                               ;
    $insertar        = ""                               ;
    $editar          = ""                               ;
    $borrar          = ""                               ;
    $sql             = ""                               ;
    $id_rol          = @ $_REQUEST[ 'id_rol'          ] ;
    $rol             = @ $_REQUEST[ 'rol'             ] ;
    $descripcion_rol = @ $_REQUEST[ 'descripcion_rol' ] ;
    $accion          = @ $_REQUEST[ 'accion' ]          ;
    $boton           = "" ;
    $color           = "" ;
    $bloqueo         = "" ;
    $limpiar         = false ;
    if ( ! $accion ) {
        $limpiar = true ;
        /*
        $accion = "agregar" ;
        $boton  = "Agregar" ;
        $color  = "success" ;
        */
    } else {
        switch ( $accion ) {
            case "ED":
            case "BO":
                $sql = "select * from rol where id_rol = ?" ;
                $consulta = $base->consultar( $sql , [ $id_rol ] ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    $rol             = $consulta[0][ 'rol'             ] ;
                    $descripcion_rol = $consulta[0][ 'descripcion_rol' ] ;
                }
                $boton   = ( $accion == "ED" ) ? "Editar"    : "Borrar" ;
                $color   = ( $accion == "ED" ) ? "secondary" : "danger" ;
                $bloqueo = ( $accion == "BO" ) ? "disabled"  : ""       ;
                $accion  = ( $accion == "ED" ) ? "editar"    : "borrar" ;
                break ;
            case "agregar":
                $sql = "
                    insert into
                    rol (
                        rol,
                        descripcion_rol
                    )
                    values (
                        ?,
                        ?
                    )
                " ;
                $insertar = $base->ejecutar(
                    $sql ,
                    [
                        $rol             ,
                        $descripcion_rol
                    ]
                ) ;
                if ( $insertar ) {
                    $limpiar = true ;
                }
                break ;
            case "editar":
                $sql = "
                    update rol
                    set
                        rol             = ? ,
                        descripcion_rol = ?
                    where
                        id_rol = ?
                ";
                $editar = $base->ejecutar( $sql , [ $rol , $descripcion_rol , $id_rol ] ) ;
                if ( $editar ) {
                    $limpiar = true ;
                } else {
                    $boton = "Editar"    ;
                    $color = "secondary" ;
                }
                break ;
            case "borrar":
                $sql = "delete from rol where id_rol = ?" ;
                $borrar = $base->ejecutar( $sql , [ $id_rol ] ) ;
                if ( $borrar ) {
                    $limpiar = true ;
                } else {
                    $boton   = "Borrar"   ;
                    $color   = "danger"   ;
                    $bloqueo = "disabled" ;
                }
                break ;
        }
    }

    if ( $limpiar ) {
        $id_rol          = "" ;
        $rol             = "" ;
        $descripcion_rol = "" ;
        $accion          = "agregar" ;
        $boton           = "Agregar" ;
        $color           = "success" ;
    }

    $sql = "select * from rol" ;
    $consultarol = $base->consultar( $sql ) ;

    if ( $consultarol && count( $consultarol ) > 0 ) {
        foreach ( $consultarol as $registro ) {
            $listaroles .= plantilla(
                "./sys/mvc/mv/root/crud-roles-lista.tpl" ,
                [
                    "IDrol"       => $registro[ 'id_rol'          ] ,
                    "ROL"         => $registro[ 'rol'             ] ,
                    "DESCRIPCION" => $registro[ 'descripcion_rol' ]
                ]
            );
        }
    }

    $_P[ 'ROLES'       ] = $listaroles          ;
    $_P[ 'ACCION'      ] = $accion              ;
    $_P[ 'BOTON'       ] = $boton               ;
    $_P[ 'COLOR'       ] = $color               ;
    $_P[ 'BLOQUEO'     ] = $bloqueo             ;
    $_P[ 'IDrol'       ] = ( $id_rol ) ? $id_rol : "" ;
    $_P[ 'ROL'         ] = ( $rol    ) ? $rol    : "" ;
    $_P[ 'DESCRIPCION' ] = ( $descripcion_rol ) ? $descripcion_rol : "" ;
    $_P[ 'MOSTRAR'     ] = "" ; // print_r($consultarol, true) ;
?>