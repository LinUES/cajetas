<!-- CRUD DE roles -->
 <h1>
    ROLES
</h1>
{MOSTRAR}
<div class='row'>
    <div class='sm-12 md-12 lg-6'>
        <form method='post'>
            <input type='hidden' name='solicitud' value='roles' >
            <input type='hidden' name='id_rol' value='{IDrol}' >
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='rol' placeholder='rol' name='rol' value='{ROL}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='descripcion_rol' placeholder='Descripción del rol' name='descripcion_rol' value='{DESCRIPCION}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row flex-spaces'>
                <div class='col-5'>
                    <input type='hidden' name='accion' value='{ACCION}'>
                    <input type='submit' class='paper-btn btn-{COLOR} btn-block' value='{BOTON}'>
                </div>
                <div class='col-5 align-middle'>
                    <a href='./' class='paper-btn btn-warning btn-block'>
                        <center>
                            Cancelar
                        </center>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class='sm-12 md-12 lg-6'>
        <table class='table-hover'>
            <thead>
                <tr>
                    <th>
                        rol
                    </th>
                    <th>
                        Descripción
                    </th>
                    <th>
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                {ROLES}
            </tbody>
        </table>
    </div>
</div>
