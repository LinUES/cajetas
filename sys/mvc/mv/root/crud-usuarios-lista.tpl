<!-- Registro de una entidad -->
<tr>
    <td>
        {CUENTA}
    </td>
    <td>
        <a href='./?solicitud=usuarios&id_cuenta={IDcuenta}&accion=roles' class='paper-btn btn-primary btn-small'>
            <i class='las la-user-tag'></i>
        </a>
        <a href='./?solicitud=usuarios&id_cuenta={IDcuenta}&accion=ED' class='paper-btn btn-secondary btn-small'>
            <i class='las la-pen'></i>
        </a>
        <a href='./?solicitud=usuarios&id_cuenta={IDcuenta}&accion=BO' class='paper-btn btn-danger btn-small'>
            <i class='las la-trash-alt'></i>
        </a>
    </td>
</tr>