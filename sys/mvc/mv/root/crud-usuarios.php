<?php
    $listacuentas        = ""                          ;
    $base                = New Conexion()              ;
    $consulta            = ""                          ;
    $insertar            = ""                          ;
    $editar              = ""                          ;
    $borrar              = ""                          ;
    $sql                 = ""                          ;
    $id_cuenta           = @ $_REQUEST[ 'id_cuenta'  ] ;
    $cuenta              = @ $_REQUEST[ 'cuenta'     ] ;
    $clave               = @ $_REQUEST[ 'clave'      ] ;
    $llave               = @ $_REQUEST[ 'llave'      ] ;
    $id_entidad          = @ $_REQUEST[ 'id_entidad' ] ;
    $id_rol              = @ $_REQUEST[ 'id_rol'     ] ;
    $habilitada          = @ ( $_REQUEST[ 'habilitada' ] ) ? 'true' : 'false' ;
    $ch_habilitada       = "" ;
    $accion              = @ $_REQUEST[ 'accion' ]     ;
    $boton               = "" ;
    $color               = "" ;
    $bloqueo             = "" ;
    $listaentidades      = "" ;
    $listaroles          = "" ;
    $idcuental           = "" ;
    $cuentar             = "" ;
    $listarolasignados   = "" ;
    $limpiar             = false ;
    $abremodal           = "" ;

    if ( ! $accion ) {
        $limpiar = true ;
        /*
        $accion = "agregar" ;
        $boton  = "Agregar" ;
        $color  = "success" ;
        */
    } else {
        switch ( $accion ) {
            case "ED":
            case "BO":
                $sql = "select * from cuenta where id_cuenta = ?" ;
                $consulta = $base->consultar( $sql , [ $id_cuenta ] ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    $cuenta  = $consulta[0][ 'cuenta'  ] ;
                    $clave   =  "" ; //$consulta[0][ 'clave'   ] ;
                    $llave   = $consulta[0][ 'llave'   ] ;
                    $ch_habilitada = ( $consulta[0][ 'habilitada' ] === true ) ? "checked" : "" ;
                }
                $boton   = ( $accion == "ED" ) ? "Editar"    : "Borrar" ;
                $color   = ( $accion == "ED" ) ? "secondary" : "danger" ;
                $bloqueo = ( $accion == "BO" ) ? "disabled"  : ""       ;
                $accion  = ( $accion == "ED" ) ? "editar"    : "borrar" ;
                break ;
            case "agregar":
                $sql = "
                    insert into
                    cuenta (
                        cuenta,
                        clave,
                        llave,
                        habilitada
                    )
                    values (
                        ?,
                        ?,
                        ?,
                        ?
                    )
                " ;
                $insertar = $base->ejecutar(
                    $sql ,
                    [
                        $cuenta ,
                        password_hash( $clave , PASSWORD_DEFAULT )  ,
                        $llave  ,
                        $habilitada
                    ]
                ) ;

                if ( $insertar ) {
                    $limpiar = true ;
                }
                break ;
            case "editar":
                if ( $clave ) {
                    $sql = "
                        update cuenta
                        set
                            cuenta     = ? ,
                            clave      = ? ,
                            llave      = ? ,
                            habilitada = ?
                        where
                            id_cuenta = ?
                    ";
                    $editar = $base->ejecutar( $sql , [ $cuenta , password_hash( $clave , PASSWORD_DEFAULT ) , $llave , $habilitada , $id_cuenta ] ) ;
                } else {
                    $sql = "
                        update cuenta
                        set
                            cuenta     = ? ,
                            llave      = ? ,
                            habilitada = ?
                        where
                            id_cuenta = ?
                    ";
                    $editar = $base->ejecutar( $sql , [ $cuenta , $llave , $habilitada , $id_cuenta ] ) ;
                    $mostrar = $base->obtenerError() ;
                }
                if ( $editar ) {
                    $limpiar = true ;
                } else {
                    $boton = "Editar"    ;
                    $color = "secondary" ;
                }
                break ;
            case "borrar":
                $sql = "delete from cuenta where id_cuenta = ?" ;
                $borrar = $base->ejecutar( $sql , [ $id_cuenta ] ) ;
                if ( $borrar ) {
                    $limpiar = true ;
                } else {
                    $boton   = "Borrar"   ;
                    $color   = "danger"   ;
                    $bloqueo = "disabled" ;
                }
                break ;
            case "roles":
            case "roles-agregar" :
            case "roles-borrar" :
                if ( $accion == "roles-agregar" ) {
                    $sql = "
                        insert into
                        rol_de_cuenta(
                            id_cuenta,
                            id_rol,
                            id_entidad
                        )
                        values(
                            ?,
                            ?,
                            ?
                        )
                    ";
                    $insertar = $base->ejecutar(
                        $sql ,
                        [
                            $id_cuenta ,
                            $id_rol    ,
                            $id_entidad
                        ]
                    ) ;
                }
                if ( $accion == "roles-borrar" ) {
                    $sql = "
                        delete from
                            rol_de_cuenta
                        where
                            id_cuenta  = ? and
                            id_rol     = ? and
                            id_entidad = ?
                    ";
                    $borrar = $base->ejecutar(
                        $sql ,
                        [
                            $id_cuenta ,
                            $id_rol    ,
                            $id_entidad
                        ]
                    ) ;
                }
                $sql      = "select * from entidad" ;
                $consulta = $base->consultar( $sql ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    foreach ( $consulta as $entidadl ) {
                        $listaentidades .= plantilla(
                            "./sys/mvc/mv/root/crud-usuarios-lista-entidades.tpl" ,
                            [
                                "IDENTIDAD" => $entidadl[ 'id_entidad' ] ,
                                "ENTIDAD"   => $entidadl[ 'entidad'    ]
                            ]
                        );
                    }
                }
                $sql = "select * from rol";
                $consulta = $base->consultar( $sql ) ;
                if ( $consulta && count( $consulta ) > 0 ) {
                    foreach ( $consulta as $roll ) {
                        $listaroles .= plantilla(
                            "./sys/mvc/mv/root/crud-usuarios-lista-roles.tpl" ,
                            [
                                "IDROL" => $roll[ 'id_rol' ] ,
                                "ROL"   => $roll[ 'rol'    ]
                            ]
                        );
                    }
                }
                $sql      = "select cuenta from cuenta where id_cuenta = ?" ;
                $consulta = $base->consultar( $sql , [ $id_cuenta ] ) ;
                $cuentar  = $consulta[0][ 'cuenta'  ] ;
                $sql      = "select * from roles_de_cuentas where id_cuenta = ?" ;
                $consulta = $base->consultar( $sql , [ $id_cuenta ] ) ;
                foreach ( $consulta as $asignados ) {
                    $listarolasignados .= plantilla(
                        "./sys/mvc/mv/root/crud-usuarios-lista-asignados.tpl" ,
                        [
                            "IDCUENTA"  => $id_cuenta ,
                            "IDROL"     => $asignados[ 'id_rol' ],
                            "ROL"       => $asignados[ 'rol'    ],
                            "IDENTIDAD" => $asignados[ 'id_entidad' ],
                            "ENTIDAD"   => $asignados[ 'entidad'    ]
                        ]
                    );
                }
                $idcuental = $id_cuenta ;
                $limpiar = true ;
                $abremodal = plantilla( "./sys/mvc/mv/root/crud-usuarios-modal.tpl" ) ;
                break ;
        }
    }

    if ( $limpiar ) {
        $id_cuenta = "" ;
        $cuenta    = "" ;
        $clave     = "" ;
        $llave     = "" ;
        $habilitada    = "" ;
        $ch_habilitada = "checked" ;
        $accion    = "agregar" ;
        $boton     = "Agregar" ;
        $color     = "success" ;
    }

    $sql = "select * from cuenta" ;
    $consulta = $base->consultar( $sql ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $registro ) {
            $listacuentas .= plantilla(
                "./sys/mvc/mv/root/crud-usuarios-lista.tpl" ,
                [
                    "IDcuenta"    => $registro[ 'id_cuenta' ] ,
                    "CUENTA"      => $registro[ 'cuenta'    ]
                ]
            );
        }
    }

    $_P[ 'CUENTAS'           ] = $listacuentas  ;
    $_P[ 'ACCION'            ] = $accion        ;
    $_P[ 'BOTON'             ] = $boton         ;
    $_P[ 'COLOR'             ] = $color         ;
    $_P[ 'BLOQUEO'           ] = $bloqueo       ;
    $_P[ 'IDcuenta'          ] = ( $id_cuenta ) ? $id_cuenta : "" ;
    $_P[ 'CUENTA'            ] = ( $cuenta    ) ? $cuenta    : "" ;
    $_P[ 'CLAVE'             ] = ( $clave     ) ? $clave     : "" ;
    $_P[ 'LLAVE'             ] = ( $llave     ) ? $llave     : "" ;
    $_P[ 'CHEQUEADO'         ] = $ch_habilitada ;
    $_P[ 'LISTAENTIDADES'    ] = $listaentidades ;
    $_P[ 'LISTAROLES'        ] = $listaroles     ;
    $_P[ 'IDcuentaL'         ] = $idcuental      ;
    $_P[ 'CUENTAR'           ] = $cuentar        ;
    $_P[ 'LISTAROLASIGNADOS' ] = $listarolasignados ;
    $_P[ 'ABREMODAL'         ] = $abremodal      ;
    $_P[ 'MOSTRAR'           ] = "" ; //print_r($error, true) ;
?>