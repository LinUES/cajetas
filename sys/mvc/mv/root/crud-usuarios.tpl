<!-- CRUD DE cuentas -->
 <h1>
    CUENTAS
</h1>
{MOSTRAR}
<div class='row'>
    <div class='sm-12 md-12 lg-6'>
        <form method='post'>
            <input type='hidden' name='solicitud' value='usuarios' >
            <input type='hidden' name='id_cuenta' value='{IDcuenta}' >
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='cuenta' placeholder='cuenta' name='cuenta' value='{CUENTA}' {BLOQUEO} required>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-12'>
                    <div class='form-group'>
                        <input type='password' class='input-block' id='clave' placeholder='Clave de la cuenta' name='clave' value='{CLAVE}' {BLOQUEO}>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col md-8'>
                    <div class='form-group'>
                        <input type='text' class='input-block' id='llave' placeholder='Llave de la cuenta' name='llave' value='{LLAVE}' {BLOQUEO} required>
                    </div>
                </div>
                <div class='col md-4'>
                    <div class='form-group'>
                        <label for='habilitada' class='paper-check'>
                            <input type='checkbox' id='habilitada' name='habilitada' value='habilitada' {CHEQUEADO} {BLOQUEO}>
                            <span>Habilitada</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class='row flex-spaces'>
                <div class='col-5'>
                    <input type='hidden' name='accion' value='{ACCION}'>
                    <input type='submit' class='paper-btn btn-{COLOR} btn-block' value='{BOTON}'>
                </div>
                <div class='col-5 align-middle'>
                    <a href='./' class='paper-btn btn-warning btn-block'>
                        <center>
                            Cancelar
                        </center>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class='sm-12 md-12 lg-6'>
        <table class='table-hover'>
            <thead>
                <tr>
                    <th>
                        Cuenta
                    </th>
                    <th>
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
                {CUENTAS}
            </tbody>
        </table>
    </div>
</div>
<div>
    <input class='modal-state' id='modal-1' type='checkbox'>
    <div class='modal'>
        <label class='modal-bg' for='modal-1'>

        </label>
        <div class='modal-body'>
            <label class='btn-close' for='modal-1'>
                X
            </label>
            <h4 class='modal-title'>
                Roles del Usuario {CUENTAR}
            </h4>
            <form method='post'>
                <input type='hidden' name='solicitud' value='usuarios'      >
                <input type='hidden' name='accion'    value='roles-agregar' >
                <input type='hidden' name='id_cuenta' value='{IDcuentaL}'   >
                <div class='row'>
                    <div class='col'>
                        <h5 class='modal-subtitle'>
                            Entidades
                        </h5>
                        <select name='id_entidad'>
                            {LISTAENTIDADES}
                        </select>
                    </div>
                    <div class='col'>
                        <h5 class='modal-subtitle'>
                            Roles
                        </h5>
                        <select name='id_rol'>
                            {LISTAROLES}
                        </select>
                    </div>
                </div>
                <div class='row'>
                    <div class='col'>
                        <input type='submit' class='paper-btn btn-small btn-success-outline' value='Asignar'>
                    </div>
                </div>
            </form>
            <h5 class='modal-subtitle'>
                Roles asigandos
            </h5>
            <div>
                <table>
                    {LISTAROLASIGNADOS}
                </table>
            </div>
        </div>
    </div>
</div>
{ABREMODAL}