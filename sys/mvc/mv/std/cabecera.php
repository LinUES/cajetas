<?php
    $TITULO = "Ejercicios PHP" ;
    setlocale(LC_ALL, 'es_SV');

    $menuprivado = "" ;
    $menupublico = "" ;

    $menupublico = plantilla( "./sys/mvc/mv/std/menu-publico.tpl" ) ;

    $hoy = date( "l d ") ;
    if ( isset( $_SESSION[ 'usuario' ] ) ) {
        $platillita = "salir"  ;
        if ( $_SESSION[ 'id_entidad' ] == 0 ) {
            $menuprivado = plantilla( "./sys/mvc/mv/std/menu-root.tpl" ) ;
        } else {
            $menuprivado = plantilla( "./sys/mvc/mv/std/menu-privado.tpl" ) ;
        }
    } else {
        $platillita = "entrar" ;
    }

    $_P[ 'TITULO'       ] = $TITULO       ;
    $_P[ 'HOY'          ] = $hoy          ;
    $_P[ 'MENUPRIVADO'  ] = $menuprivado  ;
    $_P[ 'MENUPUBLICO'  ] = $menupublico  ;
    $_P[ 'ENTRARYSALIR' ] = plantilla( "./sys/mvc/mv/std/cabecera-$platillita.tpl" ) ;
?>