<!DOCTYPE html>
<html lang='es' class=''>
    <head>
        <meta charset="utf-8" lang='es' >
        <meta name='viewport' content="width=device-width, initial-scale=1.0">
        <title>
            {TITULO}
        </title>
        <link rel='stylesheet' type='text/css' href='./usr/css/02-line-awesome.min.css'>
        <link rel='stylesheet' type='text/css' href='./usr/css/04-paper.min.css'>
        <link rel='stylesheet' type='text/css' href='./usr/css/zz-estilos.css'>
        <script src='./usr/js/zz-guiones.js'></script>
    </head>
    <body class='padding-small'>
        <nav class='border split-nav background-primary'>
            <div class='nav-brand'>
                <h3 class=''>
                    <a href='./'>
                        {TITULO}
                    </a>
                </h3>
            </div>
            <div class='collapsible'>
                <input id='collapsible-menu' type='checkbox' name='collapsible'>
                <label for='collapsible-menu'>
                    <div class='bar1'></div>
                    <div class='bar2'></div>
                    <div class='bar3'></div>
                </label>
                <div class='collapsible-body'>
                    <ul class='inline'>
                        {MENUPRIVADO}
                        {MENUPUBLICO}
                        <!--
                        <li>
                            <a href='./?solicitud=acerca'>
                                Acerca de
                            </a>
                        </li>
                        <li>
                            <a href='./?solicitud=contacto'>
                                Contacto
                            </a>
                        </li>
                        -->
                        <li>
                            {ENTRARYSALIR}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class='row padding-none'>
            <div class='col-12 col'>