<?php
    $accion  = @ $_REQUEST[ 'accion' ]   ;
    $mensaje = ""                        ;
    $visible = "invisible"               ;
    $usuario = @ $_REQUEST[ 'usuario' ]  ;
    $clave   = @ $_REQUEST[ 'clave'   ]  ;
    $cuenta  = ""                        ;
    $base    = New Conexion()            ;
    $sql     = ""                        ;

    if ( $accion == "ingresar" ) {

        include "./sys/conf/configuracion.php" ;

        if ( $usuario == $USUARIO && password_verify( $clave , $CLAVE) ) {
            $_SESSION[ 'usuario'    ] = "root" ;
            $_SESSION[ 'id_entidad' ] = 0 ;
            $_SESSION[ 'id_cuenta'  ] = 0 ;
            header( "Location: ./" ) ;
        } else {
            $cuenta = explode( "@" , $usuario ) ;
            $sql = "select clave,id_entidad,id_cuenta from roles_de_cuentas where cuenta = ? and entidad = ? and habilitada = true" ;
            $consulta = @ $base->consultar( $sql , [ $cuenta[0] , $cuenta[1] ] ) ;
            // La coparación del resultado de consulta se hace primero para evitar un mensaje en
            //la comprovación de un posible arreglo nulo en la segunda parte de la condición (cortocicuitarlo)
            if ( $consulta && password_verify( $clave , $consulta[0]['clave']) ) {
                $_SESSION[ 'usuario'    ] = $usuario ;
                $_SESSION[ 'id_entidad' ] = $consulta[0]['id_entidad'] ;
                $_SESSION[ 'id_cuenta'  ] = $consulta[0]['id_cuenta' ] ;
                header( "Location: ./" ) ;
            } else {
                $mensaje = "Credenciales incorrectas " ;
                $visible = "" ;
            }
        }

    }

    $_P['MENSAJE'] = $mensaje ;
    $_P['VISIBLE'] = $visible ;

?>