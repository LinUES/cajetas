<form method='post'>
    <div class='row flex-center'>
        <div class='md-4 sm-12'>
            <div class='card'>
                <div class='card-header text-center'>
                    <h3 class='card-title'>
                        Entrar al sitio
                    </h3>
                </div>
                <div class='card-body'>
                    <input type='hidden' name='solicitud' value='entrar' >
                    <input type='hidden' name='accion'    value='ingresar' >
                    <div class='row'>
                        <div class='md-2 sm-2 col'>
                            <i class='las la-user'></i>
                        </div>
                        <div class='md-10 sm-10 col'>
                            <div class='form-group'>
                                <input type='text' class='input-block' id='usuario' placeholder='usuario' name='usuario'>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='md-2 sm-2 col'>
                            <i class='las la-lock'></i>
                        </div>
                        <div class='md-10 sm-10 col'>
                            <div class='form-group'>
                                <input type='password' class='input-block' id='clave' placeholder='clave' name='clave'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='card-footer'>
                    <div class='row flex-spaces'>
                        <div class='col-5'>
                            <input type='submit' class='paper-btn btn-success btn-block' value='Entrar'>
                        </div>
                        <div class='col-5'>
                            <a href='./' class='paper-btn btn-warning btn-block'>
                                Cancelar
                            </a>
                        </div>
                    </div>
                    <input class='alert-state' id='alert-01' type='checkbox'>
                    <div class='alert alert-warning dismissible {VISIBLE}'>
                        {MENSAJE}
                        <label class='btn-close' for='alert-01'>
                            <i class='las la-times'></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>