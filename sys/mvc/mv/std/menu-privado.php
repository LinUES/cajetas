<?php
    $base         = new Conexion() ;
    $id_cuenta    = $_SESSION[ 'id_cuenta'  ] ;
    $id_entidad   = $_SESSION[ 'id_entidad' ] ;
    $aplicaciones = "" ;

    $sql = "
        select distinct
            aplicacion,
            etiqueta,
            archivo,
            icono,
            peso
        from
            aplicaciones_de_entidades
        where
            id_rol in (
                select
                    id_rol
                from
                    roles_de_cuentas where
                id_cuenta = ?
            ) and
            id_entidad = ? and
            publica = false and
            activa = true
        order by
            peso
    ";

    $consulta = $base->consultar( $sql , [ $id_cuenta , $id_entidad ] ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $aplicacion ) {
            $aplicaciones .= plantilla(
                "./sys/mvc/mv/std/menu-privado-elemeto.tpl" ,
                [
                    "APLICACION" => $aplicacion[ 'aplicacion' ] ,
                    "ETIQUETA"   => $aplicacion[ 'etiqueta'   ] ,
                ]
            );
        }
    }

    $_P['LISTADEELEMENTOS'] = $aplicaciones ;
?>