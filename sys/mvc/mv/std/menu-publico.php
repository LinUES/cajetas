<?php
    $base         = new Conexion() ;
    $aplicaciones = "" ;

    $sql = "
        select
            aplicacion,
            etiqueta,
            archivo,
            icono,
            peso
        from
            aplicacion
        where
            publica = true and
            activa = true
        order by
            peso
    ";

    $consulta = $base->consultar( $sql ) ;

    if ( $consulta && count( $consulta ) > 0 ) {
        foreach ( $consulta as $aplicacion ) {
            $aplicaciones .= plantilla(
                "./sys/mvc/mv/std/menu-publico-elemeto.tpl" ,
                [
                    "APLICACION" => $aplicacion[ 'aplicacion' ] ,
                    "ETIQUETA"   => $aplicacion[ 'etiqueta'   ] ,
                ]
            );
        }
    }

    $_P['LISTADEELEMENTOS'] = $aplicaciones ;
?>