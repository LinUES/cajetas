            </div>
        </main>
        <footer class='row background-primary'>
            <div class='col-12 col'>
                <div class='row'>
                <!-- Footer Location-->
                    <div class='sm-12 md-4 col'>
                        <h4 class=''>
                            Ubicación
                        </h4>
                        <p class=''>
                            En alguna parte
                            <br>
                            De algun país
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class='sm-12 md-4 col'>
                        <h4>
                            Copyleft &#127279; Gato Barato
                        </h4>
                        <a class='paper-btn btn-secondary' href='#!'>
                            <i class='lab la-fw la-facebook-f'></i>
                        </a>
                        <a class='paper-btn btn-secondary' href='#!'>
                            <i class='lab la-fw la-twitter'></i>
                        </a>
                        <a class='paper-btn btn-secondary' href='#!'>
                            <i class='lab la-fw la-linkedin-in'></i>
                        </a>
                        <a class='paper-btn btn-secondary' href='#!'>
                            <i class='lab la-fw la-dribbble'></i>
                        </a>
                    </div>
                    <!-- Footer About Text-->
                    <div class='sm-12 md-4 col'>
                        <h4>
                            Acerca del gato
                        </h4>
                        <p>
                            Gordo come lazaña, usa debian y come churros.
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>