<?php
    if ( isset( $_SESSION['usuario'] ) ) {
        unset( $_SESSION['usuario'] ) ;
        unset( $_SESSION['rol'] ) ;
        header( "Location: ./" ) ;
    }
?>