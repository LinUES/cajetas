-- Borrados
drop view if exists aplicaciones_de_entidades ;
drop view if exists roles_de_cuentas          ;

drop table if exists aplicacion_de_entidad ;
drop table if exists rol_de_cuenta         ;

drop table if exists entidad    ;
drop table if exists aplicacion ;
drop table if exists rol        ;
drop table if exists cuenta     ;

-- Creaciones
create table cuenta (
    id_cuenta  bigserial not null unique primary key,
    cuenta     text      not null unique,
    clave      text      not null,
    llave      text      not null unique,
    habilitada boolean   not null default true check ( habilitada = true or habilitada = false )
);

create table rol (
    id_rol          bigserial not null unique primary key,
    rol             text      not null unique,
    descripcion_rol text
);

create table aplicacion (
    id_aplicacion bigserial not null unique primary key,
    aplicacion    text      not null unique,
    etiqueta      text      not null,
    archivo       text      not null,
    peso          bigint    not null,
    icono         text      not null default 'las la-cog',
    seccion       boolean   not null default false check ( seccion = true or seccion = false ) ,
    publica       boolean   not null default false check ( publica = true or publica = false ) ,
    activa        boolean   not null default true  check ( activa  = true or activa  = false )
);

create table entidad (
    id_entidad          bigserial not null unique primary key,
    entidad             text      not null unique,
    nombre_entidad      text      not null,
    descripcion_entidad text
);

--- Dependientes

create table rol_de_cuenta (
    id_cuenta  bigint not null references cuenta( id_cuenta )   ,
    id_rol     bigint not null references rol( id_rol )         ,
    id_entidad bigint not null references entidad( id_entidad ) ,
    primary key ( id_cuenta, id_rol, id_entidad )
);

create table aplicacion_de_entidad (
    id_aplicacion bigint not null references aplicacion( id_aplicacion ) ,
    id_rol        bigint not null references rol( id_rol )               ,
    id_entidad    bigint not null references entidad( id_entidad )       ,
    primary key ( id_aplicacion, id_rol, id_entidad )
);

-- Vistas
create view
    roles_de_cuentas
as SELECT
    c.cuenta      ,
    c.id_cuenta   ,
    c.clave       ,
    c.habilitada  ,
    r.id_rol      ,
    r.rol         ,
    e.id_entidad  ,
    e.entidad
FROM
    cuenta as c
inner join
    rol_de_cuenta as rc
on
    c.id_cuenta = rc.id_cuenta
inner join
    rol as r
on
    rc.id_rol = r.id_rol
inner join
    entidad as e
on
    rc.id_entidad = e.id_entidad
;

create view
    aplicaciones_de_entidades
as SELECT
    e.entidad       ,
    e.id_entidad    ,
    r.id_rol        ,
    r.rol           ,
    a.id_aplicacion ,
    p.aplicacion    ,
    p.etiqueta      ,
    p.archivo       ,
    p.peso          ,
    p.icono         ,
    p.seccion       ,
    p.publica       ,
    p.activa
FROM
    aplicacion_de_entidad as a
inner join
    rol as r
on
    a.id_rol = r.id_rol
inner join
    entidad as e
on
    a.id_entidad = e.id_entidad
inner join
    aplicacion as p
on
    a.id_aplicacion = p.id_aplicacion
;

